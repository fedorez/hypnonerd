//
//  AppDelegate.h
//  HypnoNerd
//
//  Created by Denis Fedorets on 20/11/2018.
//  Copyright © 2018 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

