//
//  BNRHypnosisView.m
//  Hypnosister
//
//  Created by Denis Fedorets on 13/11/2018.
//  Copyright © 2018 Denis Fedorets. All rights reserved.
//

#import "BNRHypnosisView.h"
#import "AppDelegate.h"

@interface BNRHypnosisView()

@property (nonatomic, strong) UIColor *circleColor;

@end


@implementation BNRHypnosisView

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(nullable NSDictionary *)launchOptions
{
    
    
    return YES;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGRect bounds = self.bounds;
    // Figure out the center of the bounds rectangle
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;
    // The circle will be the largest that will fit in the view
    //float radius = (MIN(bounds.size.width, bounds.size.height) / 2.0);
    // The largest circle will circumscribe the view
    float maxRadius = hypot(bounds.size.width, bounds.size.height) / 2.0;
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
     // Add an arc to the path at center, with radius of radius,
    // from 0 to 2*PI radians (a circle)
    //[path addArcWithCenter:center
    //                radius:radius
    //                startAngle:0.0
    //                endAngle:M_PI * 2.0
    //                clockwise:YES];
    
    for (float currentRadius = maxRadius; currentRadius > 0;currentRadius -= 20) {
        [path moveToPoint:CGPointMake(center.x + currentRadius, center.y)];
        [path addArcWithCenter:center
                        radius:currentRadius // Note this is currentRadius!
                    startAngle:0.0
                      endAngle:M_PI * 2.0
                     clockwise:YES];
        
    }
    path.lineWidth = 10;
    //[[UIColor lightGrayColor] setStroke];
    [self.circleColor setStroke];
    //Draw the Line!
    [path stroke];
    //UIImage *logoImage = [UIImage imageNamed:@"Gerb_ussr.png"];
    CGRect logoRect = CGRectMake(center.x-60, center.y-60, 120, 120);
    //[logoImage drawInRect:logoRect];
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSetShadow(currentContext, CGSizeMake(4, 7),3);
    NSData *dtPicFromNet = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://www.grupoblc.com/wp-content/uploads/2015/07/images_cv_gamification.png"]];
    UIImage *imFromNet = [UIImage imageWithData:dtPicFromNet];
    //CGRect rectForPicFromNet = CGRectMake(center.x-120, center.y-120, 240, 240);
    [imFromNet drawInRect:logoRect];
    //CGContextRestoreGState(currentContext);
    //self.userInteractionEnabled = YES;
    //[self setUserInteractionEnabled:YES];
    
    
}

//When the finger touches the screen
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"%@ was touched", self);
    //Get 3 random numbers between 0 and 1
    float red = (arc4random() % 100)/100.0;
    float green = (arc4random() % 100)/100.0;
    float blue = (arc4random() % 100)/100.0;
    
    UIColor *randomColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    self.circleColor = randomColor;
}

- (void)setCircleColor:(UIColor *)circleColor
{
    _circleColor = circleColor;
    [self setNeedsDisplay];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.circleColor = [UIColor lightGrayColor];
        //[self setUserInteractionEnabled:YES];
    }
    return self;
}


@end
