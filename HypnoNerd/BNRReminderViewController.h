//
//  BNRReminderViewController.h
//  HypnoNerd
//
//  Created by Denis Fedorets on 20/11/2018.
//  Copyright © 2018 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BNRReminderViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
